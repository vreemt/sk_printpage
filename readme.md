readme.md

# Resources

* <http://stackoverflow.com/questions/18019219/pos-get-a-website-to-print-directly-to-a-defined-local-printer-s>
* <http://stackoverflow.com/questions/945691/automatically-print-image-from-website>
* <http://stackoverflow.com/questions/85019/how-can-you-make-a-web-page-send-to-the-printer-something-different-than-whats>

1. Chrome has a kiosk mode that bypasses the print dialog
1. use reference `window.open` for a `.print()` call
1. Yet another option is to have a hidden IFRAME that you call `iframe.contentWindow.print()` on. That will allow you to create an invisible layout that prints exactly as you want it to.

## HTML/Javascript Link

replace *link_to_.pdf* with the actual pdf path

	<a href="link_to_.pdf" onclick='javascript:var img=window.open("link_to_.pdf");img.print();return false;'>click</a>
